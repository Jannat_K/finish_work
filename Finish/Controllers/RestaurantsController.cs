﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Finish.Data;
using Finish.Models;
using Finish.Models.ManageViewModels;
using Finish.Models.RestaurantViewModel;
using Finish.Models.ViewModels;
using Finish.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;


namespace Finish.Controllers
{
    public class RestaurantsController : Controller
    {
  
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public readonly IHostingEnvironment _hostingEnvironment;
        private readonly FileUploadService _fileUploadService;
        private readonly PagingService _pagingService;



        public RestaurantsController(ApplicationDbContext context, PagingService pagingService, IHostingEnvironment hostingEnvironment,
            FileUploadService fileUploadService, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _pagingService = pagingService;
            _hostingEnvironment = hostingEnvironment;
            _fileUploadService = fileUploadService;
            _userManager = _userManager;
        }

        [HttpGet]
        public IActionResult Index(string name, int page = 1)
        {
            IEnumerable<Restaurant> restaurants = _context.Restaurant;
            List<IndexShowViewModel> models = new List<IndexShowViewModel>();

            foreach (var restaurant in restaurants)
            {
                models.Add(new IndexShowViewModel()
                {
                    Restaurant = restaurant,
                    Images = _context.Images.Where(l => l.RestaurantId == restaurant.Id).ToList(),
                    Reviews = _context.Review.Where(r => r.RestaurantId == restaurant.Id).ToList()
                });
            }

            PagedObject<IndexShowViewModel> objectPaging = _pagingService.DoPage(models.AsQueryable(), page);
            AllListPagingViewModel viewModel = new AllListPagingViewModel()
            {
                FilterViewModel = new FilterViewModel(name),
                PageViewModel = new PageViewModel(objectPaging.Count, page, objectPaging.PageSize),
                All = objectPaging.Objects
            };

            return View(viewModel);
        }


        //public async Task<IActionResult> Index(string searchString)
        //{
        //    var restaurant = from c in _context.Restaurant.ToList()
        //        select c;
        //    if (!String.IsNullOrEmpty(searchString))
        //    {
        //        restaurant = _context.Restaurant.Where(s => s.Name.Contains(searchString)
        //                                                    || s.Decription.Contains(searchString));
        //    }
        //    return View(restaurant);
        //}


        public IActionResult Details(int id)
        {
            Restaurant institution = _context.Restaurant.FirstOrDefault(i => i.Id == id);
            IndexShowViewModel model = new IndexShowViewModel()
            {
                Restaurant = institution,
                Images = _context.Images.Where(l => l.RestaurantId == id).ToList(),
                Reviews = _context.Review.Where(r => r.RestaurantId == id).ToList()
            };
            return View(model);

            //if (res.Images.Count > 0)
            //{
            //    var photos = new List<ImageViewModel>();
            //    foreach (var photo in res.Images)
            //    {
            //        var photoItem = new ImageViewModel()
            //        {
            //            Id = photo.Id,
            //            Name = photo.Name,
            //            Path = photo.Path,
            //            UploadDate = photo.UploadDate,
            //            RestaurantId = photo.RestaurantId,
            //            UserId = photo.UserId,
            //            UserName = photo.User.UserName
            //        };
            //        photos.Add(photoItem);
            //    }

            //    resView.Images = photos;
            //}

            //if (res.Reviews.Count() > 0)
            //{
            //    var reviewsList = new List<ReviewViewModel>();
            //    foreach (var review in res.Reviews)
            //    {
            //        var reviewItem = new ReviewViewModel()
            //        {
            //            Id = review.Id,
            //            Description = review.Description,
            //            AddDateTime = review.AddDateTime,
            //            Raiting = review.Raiting,
            //            UserId = review.UserId,
            //            UserName = review.User.UserName,
            //            RestaurantId = review.RestaurantId
            //        };
            //        reviewsList.Add(reviewItem);
            //    }

            //  resView.Reviews = reviewsList;

            //    resView.Reviews.= Math.Round(res.Reviews.Sum(r => r.Raiting) / (double) res.Reviews.Count, 1);
            //}
            //else
            //{
            //   // placeView.Reviews.Raiting = 0;
            //}

           /* return View()*/
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateViewModel model)
        {
           // ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (ModelState.IsValid)
            {
                    _context.Add(new Restaurant()
                    {
                        Name = model.Name,
                       // UserId = user.Id,
                        Decription = model.Description
                    });

                    await _context.SaveChangesAsync();
                    Restaurant restaurant = _context.Restaurant.FirstOrDefault(i => i.Name == model.Name);
                    _context.Add(new Images
                    {
                        RestaurantId = restaurant.Id
                    });

                    await _context.SaveChangesAsync();
                    Images Image = _context.Images.FirstOrDefault(l => l.RestaurantId == restaurant.Id);
                    if (model.Image!= null)
                    {
                        Image.Name = Upload(Image.Id, model.Image, "images", "avatar", "restuarn");
                    }
                    _context.Update(Image);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "Restaurants",
                        new { message = $"Place {restaurant.Name} succes" });
                
            }
            return View();
        }



        private string Upload(int supplierId, IFormFile file, string folder, string secondFolder, string thirdFolder)
        {
            var path = Path.Combine(
                _hostingEnvironment.WebRootPath,
                $"{folder}\\{thirdFolder}\\{supplierId}\\{secondFolder}");
            _fileUploadService.Upload(path, file.FileName, file);

            return $"{folder}/{thirdFolder}/{supplierId}/{secondFolder}/{file.FileName}";
        }
        //public IActionResult Create()
        // {
        //     return View();
        // }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create(PagingViewModel restaurant)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Restaurant.Add(new Restaurant
        //            {
        //                Name = restaurant.Name,
        //                Decription = restaurant.Decription,
        //                AddDate = restaurant.AddDate,
        //                MainPhoto = restaurant.MainPhoto.Name,
        //                Path = $"images/{restaurant.MainPhoto.FileName}"
        //        }

        //        );
        //        await _context.SaveChangesAsync();

        //        string path = Path.Combine(
        //            _hostingEnvironment.WebRootPath,
        //            $"images\\");

        //        _fileUploadService.Upload(path, restaurant.MainPhoto.FileName, restaurant.MainPhoto);
        //        return RedirectToAction(nameof(Index));


        //    }

        //    return View(restaurant);
        //}


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurant.SingleOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", restaurant.UserId);
            return View(restaurant);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Adress,AddDate,MainPhoto,UserId")] Restaurant restaurant)
        {
            if (id != restaurant.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restaurant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantExists(restaurant.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", restaurant.UserId);
            return View(restaurant);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurant
                .Include(r => r.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _context.Restaurant.SingleOrDefaultAsync(m => m.Id == id);
            _context.Restaurant.Remove(restaurant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurant.Any(e => e.Id == id);
        }

        //public FileResult GetFileStream(string file)
        //{
        //    string fileName = file.Split('\\')[file.Split('\\').Count() - 1];
        //    FileStream stream = new FileStream(file, FileMode.Open);
        //    string fileType = "application/octet-stream";
        //    return File(stream, fileType, fileName);
        //}

        //public void UploadFiles(Images images, List<IFormFile> formFiles)
        //{
        //    if (formFiles != null)
        //    {
        //        foreach (var file in formFiles)
        //        {
        //            var fileName = Path.Combine(_hostingEnvironment.WebRootPath + "/images/photos", Path.GetFileName(file.FileName));
        //            file.CopyTo(new FileStream(fileName, FileMode.Create));
        //            Images resPhoto = new Images() { Name = file.FileName, RestaurantId = images.RestaurantId };
        //            _context.Images.Add(resPhoto);
        //            _context.SaveChanges();
        //        }
        //    }
        //}
    }
}

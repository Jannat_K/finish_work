﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Finish.Models
{
    public class CreateViewModel
    {
        [Required(ErrorMessage = "Enter name")]
        [Display(Name = "Name")]
        public string Name{ get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Required(ErrorMessage = "Set description")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public IFormFile Image { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finish.Models
{
    public class IndexShowViewModel
    {
        public Restaurant Restaurant { get; set; }
        public List<Images> Images { get; set; }
        public List<Review> Reviews { get; set; }
    }
}

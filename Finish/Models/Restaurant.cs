﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finish.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Decription { get; set; }
        public DateTime AddDate { get; set; }
        public string MainPhoto { get; set; }
        public string Path { get; set; }
        public string UserId { get; set; }
        public virtual  ApplicationUser User { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Images> Images { get; set; }



    }
}

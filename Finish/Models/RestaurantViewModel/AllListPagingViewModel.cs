﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finish.Models.ViewModels;

namespace Finish.Models.RestaurantViewModel
{
    public class AllListPagingViewModel
    {
        public List<IndexShowViewModel> All { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}

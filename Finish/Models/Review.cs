﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finish.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Raiting { get; set; }
        public int Count { get; set; }
        public DateTime AddDateTime { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int RestaurantId { get; set; }
        public virtual Restaurant Restaurant { get; set; }
    }
}
